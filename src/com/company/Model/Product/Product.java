package com.company.Model.Product;

public class Product implements Cloneable, IProduct {

    private String productType;
    private int price;

    public Product(int price, String type) {
        this.price = price;
        productType = type;
    }

    @Override
    public int getProductPrice() {
        return price;
    }

    @Override
    public String getType() {
        return productType;
    }

    @Override
    public IProduct clone() {
        try {
            return (IProduct) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
