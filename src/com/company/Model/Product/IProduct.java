package com.company.Model.Product;

public interface IProduct {

    int getProductPrice();

    IProduct clone();

    String getType();
}
