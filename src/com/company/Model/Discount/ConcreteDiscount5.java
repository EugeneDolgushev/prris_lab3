package com.company.Model.Discount;

import com.company.Model.Product.IProduct;

import java.util.ArrayList;
import java.util.List;

public class ConcreteDiscount5 extends BaseDiscount implements IDiscount {

    private List<IProduct> products;

    public ConcreteDiscount5() {
        this.products = new ArrayList<>();
    }

    @Override
    public double getPrice() {
        double price = 0;
        for (IProduct product: products) {
            price += product.getProductPrice();
        }
        return price;
    }

    @Override
    public boolean isDiscountFull() {
        return products.size() >= 3;
    }

    @Override
    public boolean addProductToDiscount(IProduct product) {
        if (products.size() == 5) {
            return false;
        }
        if (product.getType().equals("A")) {
            return false;
        }
        if (product.getType().equals("C")) {
            return false;
        }
        products.add(product.clone());
        return true;
    }

    @Override
    public void cleanDiscount() {
        products.clear();
    }

    @Override
    public double getDiscountKoef() {
        if (products.size() == 3) {
            return 0.95;
        }
        if (products.size() == 4) {
            return 0.9;
        }
        if (products.size() == 5) {
            return 0.8;
        }
        return 1;
    }

    @Override
    public List<IProduct> getProducts() {
        return products;
    }

    @Override
    public void setProducts(List<IProduct> products) {
        this.products = new ArrayList<>();
        for (IProduct product : products) {
            this.products.add(product.clone());
        }
    }
}
