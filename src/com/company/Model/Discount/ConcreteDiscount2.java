package com.company.Model.Discount;

import com.company.Model.Product.IProduct;

import java.util.Arrays;
import java.util.List;

public class ConcreteDiscount2 extends BaseDiscount implements IDiscount {

    private IProduct productD, productE;
    private int discount;

    public ConcreteDiscount2(int discount) {
        this.discount = discount;
    }

    @Override
    public double getPrice() {
        return (productD.getProductPrice() + productE.getProductPrice()) * ((100 - discount) / 100.0);
    }

    @Override
    public boolean isDiscountFull() {
        return productD != null && productE != null;
    }

    @Override
    public boolean addProductToDiscount(IProduct product) {
        if (productD == null && product.getType().equals("D")) {
            productD = product.clone();
            return true;
        } else if (productE == null && product.getType().equals("E")) {
            productE = product.clone();
            return true;
        }
        return false;
    }

    @Override
    public void cleanDiscount() {
        productD = null;
        productE = null;
    }

    @Override
    public double getDiscountKoef() {
        return 0;
    }

    @Override
    public List<IProduct> getProducts() {
        return Arrays.asList(productD, productE);
    }

    @Override
    public void setProducts(List<IProduct> products) {

    }
}
