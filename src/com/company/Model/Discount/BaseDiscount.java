package com.company.Model.Discount;

public abstract class BaseDiscount implements Cloneable, IDiscount {

    @Override
    public IDiscount clone() {
        IDiscount discount;
        try {
            discount = (IDiscount) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
        discount.setProducts(this.getProducts());
        return discount;
    }
}
