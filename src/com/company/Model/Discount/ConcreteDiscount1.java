package com.company.Model.Discount;

import com.company.Model.Product.IProduct;

import java.util.Arrays;
import java.util.List;

public class ConcreteDiscount1 extends BaseDiscount implements IDiscount {

    private IProduct productA, productB;
    private int discount;

    public ConcreteDiscount1(int discount) {
        this.discount = discount;
    }

    @Override
    public double getPrice() {
        return (productA.getProductPrice() + productB.getProductPrice()) * ((100 - discount) / 100.0);
    }

    @Override
    public boolean isDiscountFull() {
        return productA != null && productB != null;
    }

    @Override
    public boolean addProductToDiscount(IProduct product) {
        if (productA == null && product.getType().equals("A")) {
            productA = product.clone();
            return true;
        } else if (productB == null && product.getType().equals("B")) {
            productB = product.clone();
            return true;
        }
        return false;
    }

    @Override
    public void cleanDiscount() {
        productA = null;
        productB = null;
    }

    @Override
    public double getDiscountKoef() {
        return 0;
    }

    @Override
    public List<IProduct> getProducts() {
        return Arrays.asList(productA, productB);
    }

    @Override
    public void setProducts(List<IProduct> products) {

    }
}
