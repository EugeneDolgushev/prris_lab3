package com.company.Model.Discount;

import com.company.Model.Product.IProduct;

import java.util.List;

public interface IDiscount {

    double getPrice();

    boolean addProductToDiscount(IProduct product);

    boolean isDiscountFull();

    List<IProduct> getProducts();

    void setProducts(List<IProduct> products);

    void cleanDiscount();

    IDiscount clone();

    double getDiscountKoef();
}
