package com.company.Model.Discount;

import com.company.Model.Product.IProduct;

import java.util.ArrayList;
import java.util.List;

public class ConcreteDiscount4 extends BaseDiscount implements IDiscount {

    private IProduct product, productK, productL, productM;
    private int discount;

    public ConcreteDiscount4(int discount) {
        this.discount = discount;
    }

    @Override
    public double getPrice() {
        if (productK != null) {
            return (product.getProductPrice() + productK.getProductPrice()) * ((100 - discount) / 100.0);
        } else if (productL != null) {
            return (product.getProductPrice() + productL.getProductPrice()) * ((100 - discount) / 100.0);
        } else if (productM != null) {
            return (product.getProductPrice() + productM.getProductPrice()) * ((100 - discount) / 100.0);
        }
        return 0;
    }

    @Override
    public boolean isDiscountFull() {
        return product != null && productK != null && productL != null;
    }

    @Override
    public boolean addProductToDiscount(IProduct product) {
        if (this.product == null) {
            this.product = product.clone();
            return true;
        } else if (product.getType().equals("K") && productK == null && productL == null && productM == null) {
            productK = product.clone();
            return true;
        } else if (product.getType().equals("L") && productL == null && productK == null && productM == null) {
            productL = product.clone();
            return true;
        } else if (product.getType().equals("M") && productM == null && productK == null && productL == null) {
            productM = product.clone();
            return true;
        }
        return false;
    }

    @Override
    public void cleanDiscount() {
        product = null;
        productK = null;
        productL = null;
        productM = null;
    }

    @Override
    public double getDiscountKoef() {
        return 0;
    }

    @Override
    public List<IProduct> getProducts() {
        List<IProduct> result = new ArrayList<>();
        if (product != null) {
            result.add(product);
        }
        if (productK != null) {
            result.add(productK);
        }
        if (productL != null) {
            result.add(productL);
        }
        if (productM != null) {
            result.add(productM);
        }
        return result;
    }

    @Override
    public void setProducts(List<IProduct> products) {

    }
}
