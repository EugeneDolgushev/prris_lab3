package com.company.Model.Discount;

import java.util.ArrayList;
import java.util.List;

public class DiscountManager {

    private List<IDiscount> discounts;

    public DiscountManager() {
        discounts = new ArrayList<>();
    }

    public void addDiscount(IDiscount discount) {
        discounts.add(discount);
    }

    public List<IDiscount> getDiscounts() {
        return discounts;
    }
}
