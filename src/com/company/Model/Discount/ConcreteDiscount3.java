package com.company.Model.Discount;

import com.company.Model.Product.IProduct;

import java.util.Arrays;
import java.util.List;

public class ConcreteDiscount3 extends BaseDiscount implements IDiscount {

    private IProduct productE, productF, productG;
    private int discount;

    public ConcreteDiscount3(int discount) {
        this.discount = discount;
    }

    @Override
    public double getPrice() {
        return (productE.getProductPrice() + productF.getProductPrice() + productG.getProductPrice()) * ((100 - discount) / 100.0);
    }

    @Override
    public boolean isDiscountFull() {
        return productE != null && productF != null && productG != null;
    }

    @Override
    public boolean addProductToDiscount(IProduct product) {
        if (productE == null && product.getType().equals("E")) {
            productE = product.clone();
            return true;
        } else if (productF == null && product.getType().equals("F")) {
            productF = product.clone();
            return true;
        } else if (productG == null && product.getType().equals("G")) {
            productG = product.clone();
            return true;
        }
        return false;
    }

    @Override
    public void cleanDiscount() {
        productE = null;
        productF = null;
        productG = null;
    }

    @Override
    public double getDiscountKoef() {
        return 0;
    }

    @Override
    public List<IProduct> getProducts() {
        return Arrays.asList(productE, productF, productG);
    }

    @Override
    public void setProducts(List<IProduct> products) {

    }
}
