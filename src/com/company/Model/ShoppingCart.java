package com.company.Model;

import com.company.Model.Discount.ConcreteDiscount5;
import com.company.Model.Discount.DiscountManager;
import com.company.Model.Discount.IDiscount;
import com.company.Model.Product.IProduct;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<IProduct> products;
    private List<IDiscount> activeDiscounts;
    private DiscountManager discountManager;

    public ShoppingCart() {
        products = new ArrayList<>();
        activeDiscounts = new ArrayList<>();
    }

    public void setDiscountManager(DiscountManager discountManager) {
        this.discountManager = discountManager;
    }

    public void addProduct(IProduct product) {
        products.add(product);
    }

    private void findProductsToDiscount() {
        List<IDiscount> discounts = discountManager.getDiscounts();
        for (IDiscount discount : discounts) {
            boolean canFillDiscount = true;
            while (canFillDiscount) {
                List<IProduct> selectedProducts = new ArrayList<>();
                for (IProduct product : products) {
                    if (discount.addProductToDiscount(product)) {
                        selectedProducts.add(product);
                    }
                }
                canFillDiscount = discount.isDiscountFull();
                if (canFillDiscount) {
                    if (!addDiscountToActives(discount)) {
                        canFillDiscount = false;
                    }
                    removeProducts(selectedProducts);
                }
                discount.cleanDiscount();
            }
        }
    }

    private boolean addDiscountToActives(IDiscount activeDiscount) {
        for (IDiscount discount : activeDiscounts) {
            if (discount.getClass() == activeDiscount.getClass() && activeDiscount.getClass() == ConcreteDiscount5.class) {
                return false;
            }
        }
        activeDiscounts.add(activeDiscount.clone());
        return true;
    }

    public List<IDiscount> getActiveDiscounts() {
        return activeDiscounts;
    }

    private void removeProducts(List<IProduct> products) {
        for (IProduct product : products) {
            this.products.remove(product);
        }
    }

    public double calculate() {
        findProductsToDiscount();
        return Calculator.calculate(products, activeDiscounts);
    }
}
