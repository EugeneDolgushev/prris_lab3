package com.company.Model;

import com.company.Model.Discount.ConcreteDiscount5;
import com.company.Model.Discount.IDiscount;
import com.company.Model.Product.IProduct;

import java.util.List;

public class Calculator {

    public static double calculate(List<IProduct> products, List<IDiscount> discounts) {
        double sum = 0;
        for (IProduct product : products) {
            sum += product.getProductPrice();
        }
        for (IDiscount discount : discounts) {
            if (discount.getClass() == ConcreteDiscount5.class) {
                sum += discount.getPrice();
                sum *= discount.getDiscountKoef();
            } else {
                sum += discount.getPrice();
            }
        }
        return sum;
    }
}
