package com.company;

import com.company.Model.Discount.*;
import com.company.Model.Product.Product;
import com.company.Model.ShoppingCart;

public class Main {

    private static DiscountManager discountManager = new DiscountManager();
    private static ShoppingCart shoppingCart = new ShoppingCart();

    public static void main(String[] args) {
        fillDiscounts();
        fillShoppingCart();
        shoppingCart.setDiscountManager(discountManager);
        System.out.printf("Сумма заказа: %s%n", shoppingCart.calculate());
    }

    private static void fillDiscounts() {
        ConcreteDiscount1 concreteDiscount1 = new ConcreteDiscount1(10);
        discountManager.addDiscount(concreteDiscount1);

        ConcreteDiscount2 concreteDiscount2 = new ConcreteDiscount2(5);
        discountManager.addDiscount(concreteDiscount2);

        ConcreteDiscount3 concreteDiscount3 = new ConcreteDiscount3(5);
        discountManager.addDiscount(concreteDiscount3);

        ConcreteDiscount4 concreteDiscount4 = new ConcreteDiscount4(5);
        discountManager.addDiscount(concreteDiscount4);

        ConcreteDiscount5 concreteDiscount5 = new ConcreteDiscount5();
        discountManager.addDiscount(concreteDiscount5);
    }

    private static void fillShoppingCart() {
        shoppingCart.addProduct(new Product(100, "A"));
        shoppingCart.addProduct(new Product(100, "A"));
        shoppingCart.addProduct(new Product(100, "A"));
        shoppingCart.addProduct(new Product(80, "B"));
        shoppingCart.addProduct(new Product(80, "B"));
        shoppingCart.addProduct(new Product(150, "D"));
        shoppingCart.addProduct(new Product(150, "D"));
        shoppingCart.addProduct(new Product(150, "D"));
        shoppingCart.addProduct(new Product(150, "D"));
        shoppingCart.addProduct(new Product(150, "D"));
        shoppingCart.addProduct(new Product(115, "E"));
        shoppingCart.addProduct(new Product(115, "E"));
    }
}
